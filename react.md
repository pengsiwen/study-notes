[toc]



# 1、简单配置React 项目

## 	1.1 简单实现一个react项目

 1、首先安装react react-dom

​		运行 `npm i react react-dom`

 2、在index.js中引入 （使用webpack打包辅助，不用webpack时使用 script 标签引入两个库）

~~~javascript
import React from 'react'
import ReactDom from 'react-dom'
~~~

3、使用React创建虚拟 DOM

~~~javascript
/**
 * 创建虚拟dom
 * @param {String} 参数1 创建的元素类型，标识元素的名称
 * @param {Object | null} 参数2 表示 当前这个dom元素的属性
 * @param 参数3 子节点
 * @param 参数n
 */
// <h1 title="1" id="2"></h1>
const myh1 = React.createElement('h1', {title: '1',id:'myh1'}, '这是一个dada的h1')
~~~

4、使用 ReactDom 的render方法 将虚拟dom渲染到制定容器

~~~html
<div id="app"></div>
<!--类似vue 指定一个app容器-->
~~~

~~~javascript
// 使用render方法渲染到制定容器
ReacDom.render(myh1, document.getElementById('app'))
~~~

> 如何实现标签嵌套？

~~~javascript
const myh1 = React.createElement('h1', {title: '1',id:'myh1'}, '这是一个dada的h1')
// 在父元素的虚拟dom中 传入子节点
const mydiv = React.createElement('div', {title: '2',id:'mydiv'}, '包裹myh1的div', myh1)

ReacDom.render(mydiv, document.getElementById('app'))
~~~

## 1.2 使用 jsx 语法

> jsx：在js中，混合写入类似 html 的语法，加做 jsx 语法；符合 xml 规范的js

~~~jsx
const mydiv = <div>我就是一个jsx</div>
~~~

> 为什么要使用jsx语法？

​	如上配置，每创建一个虚拟dom节点，就要使用 React.creatElemen() 来创建和标识包含关系，页面复杂度高会很麻烦？而渲染页面上的dom结构 最好的方式是写html代码。

~~~jsx
const mydiv = <div>我就是一个jsx</div>
ReacDom.render(mydiv, document.getElementById('app'))
~~~

如上来创建虚拟dom，来渲染页面，比React.creatElemen()更友好更易读 `注意：在js文件中 默认不能写这种类似于 html 的标记；否则 打包会失败`，这里可以使用babel来转换这些js中的标签

1、安装 babel 插件

​		运行 `npm i babel-core babel-loader babel-plugin-transform-runtime -D `

​		运行 `npm i babel-preset-env babel-preset-stage-0 -D`

2、安装能够识别转换 jsx 语法的 babel-preset-react

​		运行 `npm i babel-preset-react -D `

3、添加 .babelrc 配置文件

~~~json
{
      "presets": ["env", "stage-0", "react"],
      "plugins": ["transform-runtime"]
    }
~~~

4、配置loader

~~~javascript
module: {
    rules: [
        {
            test: /\.js|jsx$/,
            use: 'babel-loader',
            exclude: /node_modules/ // 注意这里要排除 node_modules 内的文件
        }
    ]
}
~~~

ok

~~~jsx
const mydiv = <div>
      我就是一个jsx
	<h1>我是一个h1</h1>
</div>
ReacDom.render(mydiv, document.getElementById('app'))
~~~

打包运行后会看到 div 和 h1 都被渲染出来了

> 这里 安装 bebel-loader 安装 7.x 的版本 npm i babel-loader@7 -D

## 1.3 学习jsx语法

1. **jsx语法的本质：**并不是直接把jsx渲染到页面上，而是内部转换成了 creactElement 形式，在渲染的；
2. **在jsx中混合写入js表达式：**在jsx语法中，要把js代码写到`{ }`中
   * 渲染数字
   * 渲染字符串
   * 渲染布尔值
   * 为属性绑定值
   * 渲染jsx元素
   * 渲染jsx元素数组
   * 将普通字符串数组，转为jsx数组并渲染到页面上【两种方案】

~~~jsx
import React from 'react'
import ReactDom from 'react-dom'

let a = 10
let str = '你好'
let boo = true
let title = '999'
const h1 = <h1>h1</h1>
const arr = [
  <h2>h2</h2>,
  <h3>h3</h3>,
  <h4>h4</h4>,
  <h5>h5</h5>

]
const strArr = [
  '毛利', '柯南', '兰'
]
//将普通字符串数组，转为jsx数组并渲染到页面上方法一
const strArr1 = []
strArr.forEach(el => {
  const temp = <h5>{el}</h5>
  strArr1.push(temp)
})
// 类似vue  {{}}

// 什么情况下使用 {} ？ 当我们需要在 jsx 控制的区域内，写 js 表达式，则需要吧 js 代码写到 {} 中
ReactDom.render(<div>
  {a + 2}
  <hr/>
  { str }
  <hr/>
  { boo ? '条件为真': '假' }
  {h1}
  <p title={title}>这是p标签</p>
  { arr }
  <hr/>
  {strArr1}
  <hr/>
  {/* //将普通字符串数组，转为jsx数组并渲染到页面上方法二 */}
  { strArr.map(el => <h3>{el}</h3>) }
</div>,document.getElementById('app'))
~~~

> 注意：循环标签数组时要加key值

3.**jsx注释**

~~~jsx
ReactDom.render(<div>
  {/**/ } // 推荐
  {
  	//             
  },
  { // } 会将后面一个 } 注释掉 语法错误 如上
</div>,document.getElementById('app'))
~~~

4.**为jsx中的元素添加class类名**：需要使用`className`来代替`class`;`htmlFor`替换label的`for`属性（class和for为js关键字，为了防止歧义替换）

5.在jsx创建DOM的时候，所有的节点，必须有唯一的根元素进行包裹

6.在jsx语法中，标签必须成对出现，如果是单标签，则必须自闭合

> 当 编译引擎，在编译jsx代码的时候，如果遇到`<`那么就把它当成HTML代码去编译，如果遇到了`{}`就把 花括号内部的代码当做 普通的js代码去编译

# 2、React中创建组件

## 2.1创建组件的方式

> **使用构造函数来创建组件，**如果要接收外界传递的数据，需要在 构造函数的参数列表中使用`props`来接收；
>
> 必须向外return一个合法的jsx创建的虚拟dom；

* 创建组件

  ~~~jsx
  function Hello() {
      // 如果 在一个组件中 return 一个 null 则表示此组件是空的，什么都不渲染
      //return null
      // 在 组件中必须返回一个合法的 jsx 虚拟dom 元素
      return <div>Hello</div>
  }
  ~~~

* 为组件传递数据

  ~~~ jsx
  const dog = {
      name: '大黄',
      age: 3,
      gender: '雄'
  }
  
  // 在构造函数中接受外界 传递过来的数据
  function Hello(props) {
    console.log(props)
    // props.name = 'zjamh'  报错 只读
    
    return <div>Hello -- {props.name} --- {props.age} -- {props.gender}</div>
  }
  
  ReactDom.render(<div>
    {/* 直接把 组件名称 以标签的形式，丢到页面上即可 */}
    {/* 使用组件并 为组件传递 peops 数据 */}
    <Hello name={dog.name} age={dog.age} gender={dog.gender}></Hello>
  </div>,document.getElementById('app'))
    
  ~~~

1.父组件向子组件传递数据

2.使用{...obj}属性扩散传递数据

~~~jsx
ReactDom.render(<div>
  {/* 直接把 组件名称 以标签的形式，丢到页面上即可 */}
  {/* <Hello name={dog.name}></Hello> */}
  <Hello {...dog}></Hello>
</div>,document.getElementById('app'))
~~~



3.将组件封装到单独的文件中

~~~jsx
// 新建一个Hello.jsx文件
import React from 'react'
export default function Hello(props) {
  return <div>Hello -- {props.name}</div>
}

~~~

~~~jsx
// index.js
// 默认，如果不做单独的配置的话，不能省略 .jsx 后缀名
import Hello from './components/Hello'
<Hello {...dog}></Hello>
~~~



4.注意： 组件名称首字母必须是大写

5.如何省略后缀名

~~~js
// webpack.config.js
resolve: {
    extensions: ['.js','.jsx', '.json'] // 表示这几个文件的后缀名可以不写
  },
~~~

## 2.2创建组件的第二种方式

> 使用class关键字来创建组件
>
> ES6 中 class 关键字，是实现面向对象编程的新形势；

注意1：在 class 的 { }区间内，只能写 构造器、静态方法和静态属性、实例方法

注意2：class 关键字内部，还是用 原来的配方实现的；所以说，我们吧class关键字，称作 语法糖

### 2.2.1了解ES6中class关键字的使用

#### 1.class中`constructor`的基本使用

~~~js
function Person (name,age) {
  this.name = name
  this.age = age
}

const p1 = new Person('盖伦', 15)
console.log(p1)
// 通过new出来的属性，访问到的属性，叫做【实例属性】
console.log(p1.name)
console.log(p1.age)
// ======================================================='

// 创建了一个动物类
class Animal {
  // 这是类中的构造器
  // 每一个类中都有一个构造器，如果没有手动指定构造器，那么可以认为类内部有个隐形的、看不见的 空的构造器 constructor () {}，手动声明构造器就会覆盖这个看不见的

  // 构造器的作用就是，每当 new 这个类的时候必然会优先会执行构造器中的代码
  // 相当于 Person 构造函数
  constructor(name,age) {
    // 实例属性 （在构造器中通过this分配的属性也可以叫做实例属性）
    this.name = name
    this.age = age
  }
}

const a1 = new Animal('大黄', 3)
console.log(a1)
~~~



#### 2.实例属性和实例方法

~~~js
function Person (name,age) {
  this.name = name
  this.age = age
}
Person.prototype.say = function() {
  console.log('这是person的实例方法')
}
const p1 = new Person('盖伦', 15)
console.log(p1)
// 通过new出来的实例，访问到的属性，叫做【实例属性】
console.log(p1.name)
console.log(p1.age)
// 这个 实例方法
p1.say()
// ======================================================='

// 创建了一个动物类
class Animal {
  constructor(name,age) {
    // 实例属性 （在构造器中通过this分配的属性也可以叫做实例属性）
    this.name = name
    this.age = age
  }
  // 这里Animal的实例方法
  jiao() {
    console.log('animal的实例方法')
  }
}

const a1 = new Animal('大黄', 3)
console.log(a1)
~~~



#### 3.静态属性和静态方法

> 【静态属性】：通过 构造函数，直接访问到的属性，叫做静态属性
>
> 【静态方法】：同理
>
> class关键字声明的 静态方法和静态属性 需要用到static关键字

~~~js
function Person (name,age) {
  this.name = name
  this.age = age
}
// 静态属性
Person.info = 'aaaaaaaa'

// 静态方法
Person.show = function() {
  console.log('这是persom的静态方法')
}
const p1 = new Person('盖伦', 15)
p1.show() // error
Person.show() //'这是persom的静态方法'
// ======================================================='

// 创建了一个动物类
class Animal {
  constructor(name,age) {
    this.name = name
    this.age = age
  }
  // 静态属性
  static info = 'eeee'
	// 静态方法
  static show() {
    console.log('这是animal的静态方法')
  }
}

const a1 = new Animal('大黄', 3)
console.log(a1)
a1.jiao()
Animal.show()
~~~



####  4.使用`extend`关键字实现继承

通过继承公共属性和方法

~~~js
// 父类
class Person{
  constructor(name,age) {
    this.name = name
    this.age = age
  }
  sayHello() {
      console.log('hello ' + this.name)
  }
}
// 子类
// 在 class 类中，可以使用 extends 关键字，实现子类继承父类
// 语法： class 子类 extends 父类 {}
class American extends Person {
    //constructor(name,age) {
      //  this.name = name
        //this.age = age
      //}
    //sayHello() {
      //  console.log('hello ' + this.name)
    //}
}
const a1 = new American('jack', 20)
console.log(a1) // Chinese{ name: 'jack', age: 20}
a1.sayHello() // hello jack
// 子类
class Chinese extends Person{
    //constructor(name,age) {
      //  this.name = name
        //this.age = age
      //}
    //sayHello() {
      //  console.log('hello ' + this.name)
    //}
}
const c1 = new Chinese('张三',22)
console.log(c1) // Chinese{ name: '张三', age: 22}
a1.sayHello() // hello 张三
~~~

#### 5. constructor构造器中super函数的使用说明

问题1：问什么要使用`super`函数

答案：在通过`extends`继承的子类中，又重新定义一个`constructor`构造器时，需要使用`super()`函数，

所以在子类中的  `constructor` 中必须优先调用一下`super()`

~~~js
// 未使用 super()重新定义子类的 constructor，会报错
class Chinese extends Person{
    constructor() {
        
    }
}
const c1 = new Chinese('张三',22)
// =====================
class American extends Person{
    constructor() {
        super()
    }
}
const a1 = new American('jack', 20) // 不报错
console.log(a1) // American {name: undefined, age: undefined}
~~~

问题2：`super`是什么东西？

答案：`super`是一个函数，而且是父类的构造器，子类中的 `super`，其实是父类中的 `constructor`构造器的一个引用

问题3：为什么调用了`super`，打印`American`的`name`和`age`是`undefined`？

答案：根据问题2，可以知道，`super()`父类构造器的引用，调用`super()`,就是调用`Person类`的`constructor()`，在定义`父类`的`constructor`我们接受了两个参数`name`和`age`，而调用`super()`时，没有传参，所以在`父类`的`constructor(name,age)`接受的`name`和`age`就都是`undefined`

~~~js
class Person{
  constructor(name,age) {
    this.name = name
    this.age = age
  }
}

class American extends Person {
  constructor() 
    super()
  }
}
const a1 = new American('jack', 20)
console.log(a1) // American {name: undefined, age: undefined}

class Chinese extends Person{
  constructor(name,age) 
    super(name,age)
  }
}

const c1 = new Chinese('张三',22)
console.log(c1) // Chinese {name: '张三', age: 22}
~~~

#### 6、为子类挂载独有的实例属性

~~~js
class Person{
  constructor(name,age) {
    this.name = name
    this.age = age
  }
  // 实例方法
  sayHello() {
    console.log('hellow')
  }
}

class American extends Person {
  constructor(name,age) {
    super(name,age)
  }
}

const a1 = new American('jack', 20)
console.log(a1)

/** 
   * 中国人
   * @class Chinese
   * @constructor
   * @param {String} name 名字
   * @param {String | Number} age 年龄
   * @param {String | Number} IDnumber 身份证号【中国人独有的】,既然是独有的，就不适合 挂载到父类上；
  */
class Chinese extends Person{
  constructor(name,age,IDnumber) {
    // this.IDnumber = IDnumber // 报错'this' is not allowed before super()
    // 在子类中 this 只能放到 super 之后使用
    super(name,age)
    this.IDnumber = IDnumber
  }
}

const c1 = new Chinese('张三',22,'130428*********')
console.log(c1)
~~~

> 注意：在子类中 this 只能放到 super 之后使用

### 2.2.2、基于 class 关键字创建组件

#### 1、最基本结构

~~~jsx
// 如果要使用 class 定义组件，必须 让自己的组件，继承自 React.Component
class 组件名称 extends React.Component {
    // 在组件内部，必须哟 render 函数
    render() {
        // render 函数中，必须返回合法的 jsx 虚拟 DOM 结构
        return <div>这是class 创建的组件</div>
        // return null // 什么都不渲染
    }
}
~~~

> render 函数的作用，是 渲染 当前组件所对应的 虚拟 DOM 元素

#### 2、class创建组件如何传参

> 在 class 关键字穿件的组件中，如果想使用 外界传递过来的 props 参数，不需要接受 ，通过 `this.props.***` 访问即可

~~~jsx
class Hello extends React.Component {
  render() {
    
    return <div>class Hello {this.props.name}</div>
  }
}
let user = {
    name: 'zs'
}
ReactDom.render(<div>
  {/* 这里 ClassHello 标签，其实是 ClassHello 类的一个实例对象其实是  */}
  <Hello name={user.name}></Hello>
</div>,document.getElementById('app'))
~~~



## 2.3两种创建组件方式的对比

> 注意：使用 class 关键字创建的组件，有自己的私有数据和生命周期函数；
>
> 注意：使用 function 创建组件，只有props,没有自己的私有数据和生命周期函数

1. 用**构造函数**创建出来的组件：叫做`无状态组件`

2. 用**class关键字**创建出来的组件：叫做`有状态组件`

3. 什么情况下使用有状态组件？什么情况下使用无状态组件？

   + 如果一个组件需要有自己的私有数据，则推荐使用：class创建的有状态组件
   + 反之推荐使用：无状态组件
   + React官方：无状态组件，由于没有自己的state和生命周期函数，所以运行效率会比 有状态组件稍微高一些

   > 有状态组件和无状态组件之间的**本质区别**就是：有误 state 属性和有无生命周期函数

4. 组件中的`props`和`state/data`之间的区别

   + props 中的数据都是外界传递过来的；
   + state/data 中的数据，都是组件私有的
   + props 中的数据都是只读的
   + state/data 中的数据都是可读可写的

### 2.3.1、介绍class创建组件中的ths.state

~~~jsx
class Hello extends React.Component {
  constructor() {
    super()
    // 私有数据
    this.state = {
      msg: '大家好'
    } 
  }
  render() {
    this.stat.msg = '我被修改了'
    return <div>class Hello {this.state.msg}</div>
  }
}
~~~

> 这个this.state = {},就相当于vue中data() { return { } }  可读写

# 3、简单的评论组件

~~~jsx
import React from 'react'
import ReactDom from 'react-dom'

function CmtItem(props) {
  return <div>
    <h1>评论人{props.user}</h1>
    <p>评论内容{props.content}</p>
  </div>
}
class CmtList extends React.Component{
  constructor() {
    super()
    this.state = {
      CommentList: [
        {id:1,user:'张三',content: '哈哈'},
        {id:2,user:'张三',content: '哈哈'},
        {id:3,user:'张三',content: '哈哈'},
        {id:4,user:'张三',content: '哈哈'},
        {id:5,user:'张三',content: '哈哈'}
      ]
    }
  }
  render() {
    return <div>
      <h1>这是评论列表组件</h1>
      {
        this.state.CommentList.map(el => <CmtItem {...el} key={el.id}></CmtItem>)
      }
    </div>
  }
}
ReactDom.render(<div>
  <CmtList></CmtList>
</div>,document.getElementById('app'))
~~~



