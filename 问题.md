### 1、微信解密手机号

* 下载cryptojs放在根目录utils下[下载cryptojs](https://github.com/gwjjeff/cryptojs/archive/master.zip)

* 在utils文件夹下命名RdWXBizDataCrypt.js文件，并写入下面的内容

* ```js
  var Crypto = require('./cryptojs/cryptojs.js').Crypto;
  var app = getApp();
  function RdWXBizDataCrypt(appId, sessionKey) {
      this.appId = appId
      this.sessionKey = sessionKey
  }
  RdWXBizDataCrypt.prototype.decryptData = function(encryptedData, iv) {
      // base64 decode ：使用 CryptoJS 中 Crypto.util.base64ToBytes()进行 base64解码
      var encryptedData = Crypto.util.base64ToBytes(encryptedData)
      // console.log(sessionKey)
      var key = Crypto.util.base64ToBytes(this.sessionKey);
      var iv = Crypto.util.base64ToBytes(iv);
      // console.log(encryptedData,key,iv)
      // 对称解密使用的算法为 AES-128-CBC，数据采用PKCS#7填充
      var mode = new Crypto.mode.CBC(Crypto.pad.pkcs7);
      try {
          // 解密
          var bytes = Crypto.AES.decrypt(encryptedData, key, {
              asBpytes: true,
              iv: iv,
              mode: mode
          });
          var decryptResult = JSON.parse(bytes);
  
      } catch (err) {
          console.log(err)
      }
      if (decryptResult.watermark.appid !== this.appId) {
          console.log(err)
      }
      return decryptResult
  }
  module.exports = RdWXBizDataCrypt
  ```

* 解密

  * ```js
    getphonenumber(e) {;
                let phone = e['mp'].detail.encryptedData;
                let iv = e['mp'].detail.iv;
                const RdWXBizDataCrypt = require('../../utils/RdWXBizDataCrypt.js');
                var appId = '';//当前小程序appId
                let session_key = wx.getStorageSync('session_key');//后台获取
                let key = session_key;
                const pc = new RdWXBizDataCrypt(appId, key);
                const data = pc.decryptData(phone, iv);
                console.log(data.phoneNumber); //当前手机号码
            }
    ```

    