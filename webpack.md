



[toc]

~~~javascript
"devDependencies": {
    "@babel/core": "^7.12.3",
    "@babel/polyfill": "^7.12.1",
    "@babel/preset-env": "^7.12.1",
    "babel-loader": "^8.1.0",
    "core-js": "^3.6.5",
    "css-loader": "^5.0.0",
    "eslint": "^7.12.1",
    "eslint-config-airbnb-base": "^14.2.0",
    "eslint-loader": "^4.0.2",
    "eslint-plugin-import": "^2.22.1",
    "file-loader": "^6.2.0",
    "html-loader": "^1.3.2",
    "html-webpack-plugin": "^4.5.0",
    "less": "^3.12.2",
    "less-loader": "^7.0.2",
    "mini-css-extract-plugin": "^1.2.1",
    "optimize-css-assets-webpack-plugin": "^5.0.4",
    "postcss-loader": "^4.0.4",
    "postcss-preset-env": "^6.7.0",
    "style-loader": "^2.0.0",
    "url-loader": "^4.1.1",
    "webpack": "^4.44.2",
    "webpack-cli": "^3.3.12",
    "webpack-dev-server": "^3.11.0"
  }
~~~







## 1.webpack是什么

**webpack**设置一个入口文件

通过入口文件形成依赖关系图，根据先后顺序引入资源 

形成**chunk (代码块)**

**chunk**根据不同资源进行不同处理

> 这个处理过程叫打包

打包完成后输出的文件为**bundle**

## 2.webpack的五个核心概念

1. Entry

   入口(Entry)指示Webpack以哪个文件为入口起点开始打包，分析内部依赖图

2. Ouput

   输出（Ouput）指示webpack打包后的资源bundles输出到哪里去，以及如何命名

3. Loader

   Loader让Webpack能够出处理那些非JavaScript文件(webpack自身只理解JavaScript)

   > 类似翻译官，将英文，日文等翻译成中文，这样中国人就能看懂了

4. Plugins

   插件(Plugins)可以用于执行范围更广的任务。插件的范围包括，从打包优化和压缩，一直到重新定义环境中的变量等

   > loader类似翻译官的工作，如果让他去开飞机，开大炮，疏通下水管道，它是做不了的。需要一些专业的人来做。而webpack就是将这些交给插件（开飞机的插件、开大炮的插件、疏通下水道插件）来做

5. Mode

   模式(Mode)指示Webpack使用相应模式的配置

   | 选项                   |                             描述                             | 特点                       |
   | ---------------------- | :----------------------------------------------------------: | -------------------------- |
   | development（开发）    | 会将**process.env.NODE_ENV**的值设为**development**。<br />启用**NamedeChunksPlugin**和**NamedModulesPlugin**。 | 能代码本地调试运行的环境   |
   | production（生产模式） | 会将process.env.NODE_ENV的值设为production。<br />启用FlagDependencyUsagePlugin，<br /> FlagIncludedChunksPlugin,<br /> ModuleConcatenation,NoEmitOnErrorsPlugin,<br /> OccurrenceOrderPlugin,SideEffectFlagPlugin和<br />  UgligyJsPlugin | 能让代码优化上线运行的环境 |

   

## 3.webpack初体验

全局安装webpack

~~~npm i webpack webpack-cli -g
npm i webpack webpack-cli -g
~~~

在本地安装

~~~
npm i webpack webpack-cli -D
~~~



~~~
根目录
	---build
	---node_modules
	---src
	 	|---index.js
	---packages-lock.json
	---packages.json
~~~



~~~javascript
// index.js
// webpack入口文件
/*
	1.运行指令
		开发环境：webpack ./src/index.js -o ./build/built.js --mode=development
		  wabpack会以 ./src/index.js 为入口文件开始打包，打包后输出到 ./build/built.js
		  整体打包环境是开发环境
		生产环境：webpack ./src/index.js -o ./build/built.js --mode=production
		  wabpack会以 ./src/index.js 为入口文件开始打包，打包后输出到 ./build/built.js
		  整体打包环境是生产环境
	2.结论
		1.webpack能处理js/json，不能处理css/img等资源  
		2.生产环境和开发环境将ES6模块化编译成浏览器能识别的模块化~
		3.生产环境比开发环境多一个压缩js代码
*/
~~~

## 4.打包样式资源

新建一个文件

~~~
根目录
	---src
		---index.css
		---index.js
~~~

首先在index.css写如一些css样式

~~~css
body,html {
    margin: 0;
    padding: 0;
    background-color: pink;
}
~~~

在index.js引入index.css

~~~javascript
// 引入样式资源
import './index.css'
~~~

> 这里webpack不能打包css样式，需要借助**loader**(帮助webpack解析它不能识别的模块)，使用loader就必须定义配置文件

这里在根目录下定义配置文件，新建一个**webpack.config.js**文件，与src同层级

~~~javascript
/*
	webpack.config.js webpack的配置文件
		作用：指示webpack 干那些活（当你运行webpack指令的时候，会加载里面的配置，以里面的配置来干活）
		
		所有的构建工具都是基于Nodejs平台运行的~模块化默认采用commomjs。
		
		src是写项目代码   用的ES6模块化
		webpack是写配置代码	基于node平台，所以用的commonjs模块化
		这两个是两方面
*/
const { resolve } = reuqire('path') // 引入nodejs的path模块,通过解构赋值来提取reslove
// resolve是一个专门处理（拼接）绝对路径的一个方法

// commonjs 的 module.exports 去暴露一个对象
module.exports = {
    // webpack配置 
    // 先写入上面五个核心配置
    
    entry: './src/index.js', // 入口起点 指示从./src/index.js开始打包
    output: { // 输出  注意：输出是一个对象，有两个属性
        filename: 'build.js', // 输出的文件名
        /*
           path: ''输入的路径 这里输出的build文件下面, 需要在src同层级下新建一个build文件夹
           通常来讲这里会写个绝对路径（避免出错），需要借助node的一个核心模块path
           
           __dirname：nodejs的变量，代表当前文件（webpack.config.js）的目录的绝对路径（这里webpack路径就是 ./根目录）
           
        */
        path: resolve(__dirname,'build') // 输出到webpack.config.js路径的build文件夹下面（./根目录/build）
    },
    // laoder配置
    module: {
        rules: [
            // 详细的loader配置
            // 不同文件配置不同loader处理
        ]
    },
    // plugins的配置
    plugins: [
        // 详细plugins的配置
    ],
    // 模式
    mode: 'development' // 开发模式
    // mode: 'production' // 生产
}

~~~

> 以上就做了一个简单的配置搭建，还不能处理css样式，需要使用一个东西**css-loader**

如何使用

~~~javascript
// webpack.config.js
// laoder配置
module: {
    rules: [
        {
            // 匹配那些文件 通常使用正则匹配
             test: /\.css$/, // 匹配文件名以.css结尾,会遍历所有文件，一旦发现.css文件就会进行处理
            // 使用use进行处理,使用那些loader进行处理
            use: [ 
                // use数组中loader执行顺序：从右到左，从下到上依次执行
                // 处理样式文件需要以下两个loader
                'style-loader', // 创建一个style标签，将js中的样式资源插入进去，添加到head中生效
                'css-loader' // 将css文件变成commonjs模块加载js中，里面的内容是样式字符串
            ]
        }
    ]
},
~~~

配置完成后下载依赖

首先我在根目录下**npm init**，初始化一个包描述文件

然后下载weback和webpack-cli开发依赖

~~~
npm i webpack webpack-cli -D
~~~

再下载style-loader和css-loader依赖

~~~
npm i style-loader css-loader -D
~~~

依赖下载完成后运行webpack命令,就会开始打包

~~~
webpack
~~~

在这里我运行webpack时报错

**webpack : 无法将“webpack”项识别为 cmdlet、函数、脚本文件 或可运行程序的名称。请检查名称的拼写，如果包括路径，请确 保...**

我的解决办法是将webpack和wabpack-cli全局安装，在运行webpack命令就顺利打包了

~~~
npm install webpack -g
npm install webpack-cli -g
~~~





运行完成之后，在build文件夹下会多出一个build.js文件

![1603252618(1)](F:\我创建的例子项目\webpack笔记\1603252618(1).png)

> ​	在build.js中会看到如上所示,写的样式，css-loader 将其变成commonjs模块加载js中，里面的内容是样式字符串

这时在build文件夹中添加一个index.html文件，引入build.js，在浏览器中的打开

~~~html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
   <script src="./build.js"></script>
</body>
</html>
~~~

![1603252967(1)](F:\我创建的例子项目\webpack笔记\1603252967(1).png)

> 这就是'style-loader'的作用： 创建一个style标签，将js中的样式资源插入进去，添加到head中生效

以上就是简单的webpack打包样式



处理less资源

~~~javascript
module: {
    rules: [
        {
             test: /\.less$/,
            use: [ 
                'style-loader', 
                'css-loader',
                // 需要下载 less-loader和less
                'less-loader' // 将less文件编译成css文件
            ]
        }
    ]
},
~~~

## 5.webpack打包html资源

​		上面处理样式资源时使用的是loader，loader需要先下载，再使用（配置loader）

​		处理html资源时我们需要用到插件（plugins）,plugins也是需要先下载，然后是需要引入，其次是使用

> 处理html资源所用的插件是**html-webpack-plugin**

~~~
npm i html-webpack-liugin -D  // 下载
~~~

创建文件

~~~
根目录
	---build
	---src
		---index.html
		---index.js
	---webpack.config.js
~~~

~~~html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  <h1>Hello html</h1>
</body>
</html>
~~~

~~~javascript
function add(num,num2) {
    return num + num2
}
console.log(add(3,3))
~~~



~~~javascript
const { resolve } = reuqire('path')
// 下载后，这里引入htnml插件
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    entry: './src/index.js',
    output: {
        filename: 'build.js',
        path: resolve(__dirname,'build') 
    },
    module: {
        rules: []
    },
    plugins: [
        // html-webpack-plugin
        // HtmlWebpackPlugin因为它是一个构造函数，使用new 调用
        // 功能：默认会创建一个空的HTML，自动引入打包输出的所有资源（js/css）
        new HtmlWebpackPlugin()
    ],
    // 模式
    mode: 'development' // 开发模式
    // mode: 'production' // 生产
}

~~~

​		这样就简单的配置好了使用webpack命令，就会在build文件夹下多出两个文件，一个build.js和一个index.html，打开index.html文件会发现自动引入了build.js不需要再入上面一样，需要自己创建index.html文件，手动引入build.js

~~~html
  <!-- ./build/index.html -->
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
    <!-- 这就是自动添加的，不是手动添加的 -->
    <script type="text/javascript" src="build.js"></script>
</body>
</html>
~~~

> 这里会发现src/index.html和build/index.html，除了build下自动引入了build.js，src/index.html的<h1>的内容在build/index.html中不存在 ，但add()方法还是会执行

> 需求：需要有结构的HTML文件。就是说不只要自动引入build.js。我们编写的html结构也要打包处理出来

~~~javascript
plugins: [
        new HtmlWebpackPlugin({
            // 使用template属性，就会复制'./src/index.html'文件，并自动引入打包输出的所有资源
            template: './src/index.html'
        })
    ],
~~~

这样配置完成后运行webpack命令，在打开build/index.html文件，就会发现有src/index.html的结构了

~~~html
  <!-- ./build/index.html -->
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
    <h1>Hello html</h1>
    <!-- 这就是自动添加的，不是手动添加的 -->
    <script type="text/javascript" src="build.js"></script>
</body>
</html>
~~~

> 这样结构既有了html结构，add()方法也会执行



## 6.webpack打包图片资源

> 打包图片资源需要用到两个loader: url-loader, file-loader

~~~javascript
// webpack基本配置
const {resolve} = require('path')
const HtmlWebpackPlugin = reuqire('html-webpack-plugin')
module.exports = {
    entry: './src/index',
    output: {
        fileaname: 'build.js',
        path: resolve(__dirname, 'build')
    },
    module:{
        rules: [
            {
                test: /\.(jpg|png|gif)$/,
                // 这里只使用一个loader处理时，可以不用使用数组的形式，直接使用laoder:'loader名'的形式
                // 这里能因为url-loader是依赖于file-loader的，所以需要下载两个loader
                loader: 'url-loader',
                //options是用于对loader进行配置
                options: {
                    // 打包图片的时候不会原封不动的输出
                    // 这个limit配置是：当图片大小 小于 32kb，就会被base64处理
                    // 优点：减少请求数量（减轻服务器压力）
                    // 缺点: 图片体积会更大（base64）(文件请求变慢)
                    limit: 32 * 1024
                }
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html'
        })
    ],
    mode: 'development'
}
~~~

​	

​		这样就配置好了一个打包图片资源的loader，运行webpack命令后，会在build文件下看到处理后的打包文件，其中图片文件的名称是一串长字符，是hash值

​		但是这用配置只能处理css中的图片引入（**backgroung-img:url('....')**）,对于html文件中的图片引入（**img**）是处理不了的，只会原封不动的复制过去

~~~html
// build文件下的html文件
<img src="./01.html">
// 这里 如果说按上述配置了webpack,打包完成后，src就相对路径，这build文件下是找不到为01的图片的。
~~~

​	所以说要处理html文件中的图片引入问题，还需要借助一个loader来处理**html-loader**

~~~javascript
module:{
        rules: [
            {
                test: /\.(jpg|png|gif)$/,
                loader: 'url-loader',
                options: {
                    limit: 32 * 1024,
                    //关闭url-loader的es6模块化
                    esModule: false,
                    // 给图片重命名
                    // [hash:10] 去图片的hash前10位
                    // [ext] 文件的原扩展名
                    name: '[hash:10].[ext]'，
                    outputPath: 'img'
                }
            },
            {
                test: /\.html$/,
                // 处理html文件中的img图片引入（负责引入这个图片，从而能被url-loader处理）
                laoder: 'html-loader'
            }
        ]
    }
~~~

> 这里运行webpack处理时，还会有一个问题，打包后img图片的src出错了，这是因为url-loader默认使用es6模块解析，而html-loader引入图片是commonjs，解决：关闭url-laoder的es6模块化，使用commonjs

~~~html
<!--build/index.html-->
<img src="[object Moudle]">

~~~

## 7.如何打包其他资源

例如：字体资源，字体图标资源等等

这些资源不需要做其他处理，原封不动输出出去，这些资源就属于其他资源

处理其他资源统一使用**file-loader**处理

~~~javascript
//在入口文件index.js中引入要处理的其他资源
import './index.css' // css中包含了字体资源

// webpack.config.js中的loadere配置
module: {
    rules: [
        {
            test:/\.css$/,
            use: ['style-loader', 'css-loader']
        },
        // 打包其他资源(除了html/css/js资源以为的其他资源)
        {
            // 使用exclude排除css/html/js资源
            exclude:/\.(html|css|js)$/,
            loader: 'file-loader'
        }
    ]
}
~~~

## 8.devServer

​	能帮助我们开发时有修改代码，不用一次又一次重新打包编译，会帮助我们自动编译	

​	这里需要**webpack-dev-server**，需要下载

> npm i webpack-dev-server  -D

~~~javascript
// webpack.condig.js

// 开发服务器 devServer：用来自动化（自动编译，自动打开浏览器，自动刷新）
// 特点只会在内存中编译打包，不会有任何输
// 启动devServer指令为npx webpack-dev-server
devServer: {
    // 代表运行的项目目录
    contentBase: resolve(__dirname, 'build'),
    // 启动gizp压缩
    compress:true,
    // 端口号
    prot: 9000
}
~~~

## 9.单独提取css文件

​	使用**mini-css-extract-plugin**插件

​	在处理css文件时，用到了两个loader**style-loader**和**css-loader**

​	css-loader是处理css文件写到js文件中。再由style-loader在html中创建style标签写入样式。

​	而mini-css-extract-plugin是从js文件中提取css样式，单独处理到css文件中，所以不需要再使用style-loader。mini-css-extract-plugin里内置了处理css文件的loader，替换掉style-loader即可

~~~javascript
const MiniCssExtractPlugin = reuqire('mini-css-extract-plugin')

// 替换style-loader
// 'style-loader' => MiniCssExtractPlugin.loader

// 在plugins中添加插件
new MiniCssExtractPlugin()

// 对输出的css文件的位置和名字处理
new MiniCssExtractPlugin({
    // 输出到对应出口文件下，创建css文件夹放入输出的名为build.css问文件
	filename: 'css/build.css'
})
~~~

> 打包处理后的html文件会自动映入css下的build.css

## 10.css兼容性处理

webpack对css进行兼容处理如要用到一个库：**postcss => postcss-loader post-preset-env**

这里需要npm下载这两个包，然后就可以在webpack.config.js中处理

~~~
npm i  postcss-loader post-preset-env -D
~~~

~~~javascript
// webpack。config.js
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
function resolve(dir) {
  return path.join(__dirname, dir)
}

// 设置node环境变量,修改默认，使兼容处理以开发模式为准
process.env.NODE_ENV = 'development'

module.exports = {
  entry: './src/js/index.js',
  output: {
    filename: 'js/build.js',
    path: resolve('build')
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [ 
          MiniCssExtractPlugin.loader,
          'css-loader',
          // css兼容处理需要使用一个库：postcss => postcss-loader postcss-preset-env
          // postcss-preset-env这个插件能帮助我们识别某些环境，从而加载某些配置，能让我们的兼容性精确的浏览器的某一个版本
          // 帮助postcss找到package.json中browserlist里面的配置，透过配置加载制定的css兼容性样式


          /**
           *  "browserslist": {
           *  开发环境 --> 设置node环境变量: process.env.NODE_ENV = development
                "development": [
                  "last 1 chrome version",
                  "last 1 firefox version",
                  "last 1 safari version"
                ],
                "production": [
                  生产环境：默认看生产环境
                  ">0.2%",
                  "not dead",
                  "not op_mini all"
                ]
              }
           */

          // 使用loader的默认配置
          // 'postcss-loader'
          // 修改loader配置
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: {
                plugins: [['postcss-preset-env', {}]]
              }
            }
          }
        ]
      }
    ]
  },
  plugins: [
    //处理html模板
    new HtmlWebpackPlugin({
      template: './src/index.html'
    }),
    new MiniCssExtractPlugin({
      filename: 'css/build.css'
    })
  ],
  mode: 'development'
}
~~~

 webpack.config.js配置好了还需要配置browsetslist: {}

> 关于**browsetslist**的具体配置可以到GitHub上搜索

~~~json
"browserslist": {
    "development": [
      "last 1 chrome version",
      "last 1 firefox version",
      "last 1 safari version"
    ],
    "production": [
      ">0.2%",
      "not dead",
      "not op_mini all"
    ]
  }
~~~

> 关于post-preset-env配置browserslist，版本不同也有不同的配置，可以自行搜索其他配置方法

## 11.压缩css

压缩css只需要用到一个插件：**optimize-css-assets-webpack-plugin**

只需要在webpack.config.js中使用这个插件就可以

~~~javascript
// 先引用
const optimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin')

//使用
plugins: [
    ... // 其他插件
    new optimizeCssAssetsWebpackPlugin()
]
~~~

如上配置完成后就可以运行webpack处理，压缩css文件了

## 12、js语法检查

首先进行js语法检查是借助loader,所以需要想在相应依赖。**eslint-loader**，这和loader又依赖**eslint**这个库。

> 注意：eslint只检查自己写的代码，第三方库是不检查的

设置规则：

​	在package.json添加eslintConfig配置

~~~json
"eslintConfig": {
   "extends": "airbnb-base"
}
~~~

​	推荐使用airbnb规则，airbnb对应需要的依赖： eslint-config-airbnb-base、eslint、eslint-plugin-import

> airbnb规则可以在GitHub上查看详细规则，对应使用的依赖是使用方法可以在npmjs.com中搜索eslint

~~~javascript
const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require('path');

function resolve(dir) {
  return path.join(__dirname,dir)
}
module.exports = {
  entry: './src/js/index.js',
  output: {
    filename: 'js/build.js',
    path: resolve('build')
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/, // 排除node_modules内的第三方库，不检查，会报错
        loader: 'eslint-loader',
        options: {
          // 自动修复eslint错误
          fix: true
        }
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html'
    })
  ],
  mode: 'development'
}
~~~

设置了**fix：true**会在源代码中自动修复格式错误

在使用airbnb规则，在js中使用**console.log()**时会爆出警告，但不会影响运行，这里需要借助**eslint-disable-next-line**来告诉eslint这下一行eslint规则失效，**eslint-disable-next-line**是以注释的方式使用

~~~javascript
// 下一行eslint所有规则都失效
// eslint-disable-next-line
console.log('hello eslint'); // 这一行eslint的规则就不会生效
~~~

## 13、js兼容处理

首先js兼容性处理需要用到三个依赖：babel-loader、@babel/preset-env、@babel/core

~~~
npm i babel-loader @babel/preset-env @babel/core -D
~~~



js兼容处理总的来说有三种方法：

**1、基本的js兼容处理：---> @babel/preset-env；**

~~~javascript
// index.js
const add = (x,y) => {
    return x + y
}
console.log(add(1,2))

const promise = new Promise((resolve) => {
  setTimeout(() => {
    console.log('aaaaaaaaaaaa')
    resolve()
  }, 1000)
})
console.log(promise)
~~~



~~~javascript
module: {
    rules: [
        {
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            options: {
                //  预设：指示babel做怎样的兼容性处理
                presets: ['@babel/preset-env'] // 这里做了一个基本的预设环境的兼容性处理
            }
        }
    ]
}

~~~

如上在loader中配置babel就做好了一个简单兼容性处理，运行webpack命令后，查看输出的js文件

![1](F:\我创建的例子项目\webpack笔记\1.PNG)

会看到const被转为var,箭头函数被转为function等兼容性写法

> 问题：这种方法只能转换基本语法，如promise等高级语法是不能转换的，如上图，promise没有被转换，指示还是只转换了const和箭头函数

**2、全部js兼容性处理 ---->@babel/polyfill**

全部兼容性处理需要用到@babel/polyfill这个依赖

~~~
npm i @babel/polyfill -D
~~~

使用方法：

​		不需要在webpack.config.js中引用，而是在js文件中引用，如下

~~~javascript
// index.js

import '@babel/polyfill'


const add = (x,y) => {
    return x + y
}
console.log(add(1,2))

const promise = new Promise((resolve) => {
  setTimeout(() => {
    console.log('aaaaaaaaaaaa')
    resolve()
  }, 1000)
})
console.log(promise)
~~~

import引用后即可，运行webpack打包后，打开build.js(打包后的js文件)。就会发现build.js文件体积变大，其中多了很多兼容处理。这是因为@babel/polyfill，将所有兼容性处理全部引入进来了，比较暴力的处理方法

> 问题：我们只需要解决我们需要的兼容处理，如果将所有兼容代码全部引入，文件体积变大

**3.js兼容处理按需加载 ---->core-js**

~~~
npm i core-js -D
~~~

> 注意：这种方法不能和第二种方法同时使用，需要注释掉@babel/polyfill的引用

配置如下

~~~javascript
module: {
    rules: [
        {
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            options: {
                presets: [
                    [
                        '@babel/preset-env',
                        {
                            // 按需加载
                            useBuiltIns: 'usage',
                            // 指定core-js版本
                            corejs:{
                                version: 3
                            },
                            // 指定兼容性做到那个版本的浏览器
                            targets:{
                                chrome: '60',
                                firefox: '60',
                                ie: '9',
                                safari: '10',
                                edge: '17'
                            }
                        }
                    ]
                ]
            }
        }
    ]
},
~~~

> 注意：presets的配置,和第一种方法有些不一样：presets：[ [ ...这里写loader配置 ] ]，两个数组

然后运行webpack命令就可以做到兼容性按需加载了~

## 14、压缩html和js代码

这两部分的压缩很简单

首先js代码压缩：生产环境下（mode:'droduction'）,会自动压缩js代码

~~~javascript
mode: 'droduction'
~~~

> 在前面学到过在生产环境下，会加载很多插件，其中有一个插件为**UglifyJsPlugin**会自动压缩js代码，无需关注其他

然后是压缩html代码：这里我们只需要在**HtmlWebpackPlugin**插件中添加一个新的配置**minify**即可

~~~javascript
 plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html',
      // 压缩html代码
      minify: {
        collapseWhitespace: true, // 移除空格
        removeComments: true, // 移除注释
      }
    })
  ],
~~~

如上配置就可以实现html代码压缩

## 15、关于webpack的性能优化

* 开发环境性能优化

* 生产环境性能优化

  ### 开发环境性能优化

  * 优化打包构建速度
    * HMR
  * 优化代码调试
    * source-map

  ### 生产环境性能优化

  * 优化打包构建速度
    * oneOf
    * babel缓存
    * 多进程打包
    * externals
    * dll
  * 优化代码运行的性能
    * 缓存（hash、chunkhash、contenthash）
    * tree shaking
    * code split
    * 懒加载/预加载
    * pwa

## 16、开发环境打包构建优化（HMR）

在以前学习了webpack-dev-server，用它来实现代码修改热更新，但是有一个问题是，当我们只修改了其中一个模块时（某个css\某个js），就会将所有的模块全部重新构建，影响构建速度，所以要用到**HMR**

**HMR: hot module replacement 热模块替换/模块热替换**

> 作用：一个模块发生变化，只会重新打包这一个模块(而不是打包所有模块)极大提升构建速度

实现HMR其实很简单，只需要在**devServer**中添加**hot:true**即可
~~~javascript
// webpack.config.js
devServer: {
	...
	hot: true, // 开启HMR功能
}
~~~
问题：
1、修改样式文件时，HMR正常使用，只重新构建了修改过的css文件，因为style-loader内部实现了HMR功能~
2、js文件：发现js文件默认不适用HMR功能
	解决方法：需要修改js代码，添加支持HMR功能的代码
~~~javascript
import print from './print.js'
// 会全局寻找module这个对象，查看hot热更新(HMR)是否启用 ---> 让HMR功能代码生效
if(module.hot) {
	module.hot.accept('./print.js', function{
	// 方法会监听print.js文件的变化，一旦发生变化，其他模块不会打包构建，会执行后面的回调函数
		print()
	})
}
~~~
>注意：HMR功能对js处理，只能处理非入口js文件的其他文件（入口文件改变，则必然全都重新构建）

3、HTML文件：也是默认不支持HMR功能的，但会导致htmlwe文件不能热更新了~~
解决办法：修改entry入口，将html文件引入
~~~javascript
entry: ['./src/js/index.js', './src/index.html'],
~~~
思考：html文件需不需要HMR呢？
	html文件对应来讲只有一个html文件，在看下HMR的作用，一个模块发生变化，只会重新打包这一个模块。不像js文件有很多个模块，其中一个变其他不变。html文件变化，只能变化着一个文件，所以没必要使用HMR功能



## 17、开发环境调试优化（source-map）

> source-map: 一种提供源代码到构建后代码映射技术（如果构建后代码出错，通过映射可以追踪源代码错误）

配置方法是在webpack.config.js中增加一个配置**devtool:'source-map'**
~~~javascript
devtool: 'source-map'
~~~
如上配置，就配置好了一个调试优化，执行webpack命令后，会发现和build.js同一目录下多了一个**map** 文件，这个就是所谓的source-map文件，提供源代码到构建后代码映射关系

> devtool：有几个参数: [ inline- |hidden -| eval- ] [ nosources- ] [ cheap - [ module - ] ] source-map 

| 参数                    | map文件生产方式 | 介绍                                                         |
| ----------------------- | --------------- | ------------------------------------------------------------ |
| source-map              | 外部            | 错误代码的准确信息 和 源代码的错误位置                       |
| inline-source-map       | 内联            | 只生成一个内联source-map；错误代码的准确信息 和 源代码的错误位置 |
| hidden-source-map       | 外部            | 错误代码的错误原因，但是没有错误位置； 不能追踪到源代码错误，只能提示到构建后代码的错误位置 |
| eval-source-map         | 内联            | 每个文件狗生成对应的source-map,都在eval；错误代码的准确信息 和 源代码的错误位置 |
| nosources-source-map    | 外部            | 错误代码的准确信息，但是没有任何源代码信息                   |
| cheap-source-map        | 外部            | 错误代码的准确信息 和 源代码的错误位置；只能精确到行         |
| cheap-module-source-map | 外部            | 错误代码的准确信息 和 源代码的错误位置；module会将loader的source-map加入 |

内联：sorce-map文件是和build.js合二为一
外部：在build.js同级生产map文件
> 内联 和 外部的区别： 1、外部生成了文件，内联没有。2、内联构建速度更快

~~~
   开发环境：速度快一点，调试更友好
     速度快（eval> inline>cheap>...）
      		eval-cheap-souce-map
     	 	eval-source -map
      调试友好
        	source-map
        	cheap-moudle-sourec-map
        	cheap-sourec-map
        
      开发环境最好的选择：---> eval-source-map / eval-source-cheap-moudle-map
    生产环境：源代码要不要隐藏？调试要不要更友好
      内联会让代码体积非常大，所以生产环境不用内联
      	nosources-source-map 全部隐藏
      	hidden-source-map 只隐藏源代码，会提示构建后代码错误

    生产环境最好的选择：---> source-map / cheap-moudle-sourec-map
   
~~~

## 18、生产环境构建优化（oneOf）

​		首先配置是很简单，如下

~~~javascript
module: {
    rules: [
        {
            oneOf: [
				// 对应loader配置
            ]
        }
    ]
}
~~~

为什么使用oneOf？

​	在我原先写loader的时候，在rules中有非常多的loader规则，一个文件要被所有的loader都过一遍，有些loader处理不了，有些loader则会被命中，这样就不太好。配置oneOf能让loader处理性能更好（oneOf主要是提升构建速度，是文件不会被多个loader反复都过一遍）

> oneOf的意思：以下loader只会匹配一个
>
> 注意：不能有两项配置，处理同一类型的文件，如下，js类型文件有两种配置，只会生效一个loader配置。
>
> 解决办法：将eslint-loader提取出去，如下。这样rules内的两个loader都会执行，配置了enforce: 'pre'，也会优先执行

~~~javascript
module: {
    rules: [
        {
            test: /\.js$/,
            exclude: /node_modules/, // 排除node_modules,
            loader: 'eslint-loader',
            // 设置优先执行
            enforce: 'pre',
            options: {
              fix: true
            }
        },
        {
            oneOf: [
				//{
                  //  test: /\.js$/,
                   // exclude: /node_modules/, // 排除node_modules,
                   // loader: 'eslint-loader',
                    // 设置优先执行
                   // enforce: 'pre',
                   // options: {
                     // fix: true
                    //}
                  //},
                  // js兼容处理..
                  {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    loader: 'babel-loader',
                    options: {
                      presets: [
                        [
                          '@babel/preset-env',
                          {
                            // 按需加载
                            useBuiltIns: 'usage',
                            corejs: {version: 3},
                            targets: {
                              chrome: '60'
                            }
                          }
                        ]
                      ]
                    }
                  }
            ]
        }
    ]
}
~~~

## 19、生产环境构建优化（缓存）

从两点出发来设置缓存：1、babel缓存；2、整体资源缓存

1、babel缓存是什么意思呢？

​	我们写代码的时候，永远是js代码是最多的，结构和样式没有什么办法做更好的处理，为什么对babel处理，因为babel对js进行编译处理，编译成浏览器能识别的语言（js兼容性处理）。假设100个js模块，只改了1个js文件，不可能将全部文件重新编译处理，应该是不变的，类似前面的HMR功能，一个某块变，其他模块不变。生产环境先不能是使用webpack-dev-server所以不能使用HMR功能。配置如下，只需要在babel-loader中options添加一个熟悉

~~~javascript
{
    test: /\.js$/,
        exclude: /node_modules/,
            loader: 'babel-loader',
                options: {
                    // 开启babel缓存
                    // 第二次构建时，才会读取缓存
                    cacheDirectory: true,
                    presets: [
                        [
                            '@babel/preset-env',
                            {
                                // 按需加载
                                useBuiltIns: 'usage',
                                corejs: {version: 3},
                                targets: {
                                    chrome: '60'
                                }
                            }
                        ]
                    ]
                }
}
~~~

2、文件资源缓存

​	怎么查看缓存呢？首先先写一个简单的服务器代码server.js

~~~javascript
// server,js
/*
	服务器代码
	通过node server.js运行
	
	访问服务器地址
		http://loacalhost:3000
*/
const express = require('express')

const app = express()

app.use(express.static('build', {maxAge: 1000 * 3600}))

app.listen(3000)
~~~



 ![2](F:\我创建的例子项目\webpack笔记\2.PNG)<img src="F:\我创建的例子项目\webpack笔记\3.PNG" alt="3" style="zoom:75%;" />

当我修改了代码是，访问的还是上一次缓存的内容，修改的代码没有生效

可以通过文件名添加版本号（hash值）的方式来解决这个问题（如果资源名称没有变就走缓存，如果变了就会重新请求资源文件）

> hash:每次webpack构建时会生成一个唯一的hash值

~~~javascript
...

module.exports = {
  entry: './src/js/index.js',
  output: {
    filename: 'js/build.[hash:10].js',
    path: resolve('build')
  },
  module: {
    ...
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'css/build.[hash:10].css'
    }),
    ...
  ],
  mode: 'production'
}
~~~

如上配置，将输出的文件名添加10位的hash值，但是这样会有一个问题：因为css文件和js文件是共享的webpack打包生成的hash值，一旦发生变化css文件和js文件是一起变的，修改一个都会变，缓存就失效了。webpack又引入了另一个hash值chunkhash

> chunkhash:根据chunk生成的hash值，如果打包来源同一个chunk，那么hash值就一样

~~~javascript
...
module.exports = {
  entry: './src/js/index.js',
  output: {
    filename: 'js/build.[chunkhash:10].js',
    path: resolve('build')
  },
  module: {
    ...
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'css/build.[chunkhash:10].css'
    }),
    ...
  ],
  mode: 'production'
}
~~~

这里打包会发现js和css的hash值还是一样的，因为css是在js中被引进来的，所以同属于一个chunk。因为这些都被引入到同一个入口文件，所有根据入口文件引入的都会生成一个chunk。

所以最后的解决方法就是通过contenthash来解决

> contenthash: 根据文件的内容来生成hash值。不同的hash值一定不一样

~~~javascript
...
module.exports = {
  entry: './src/js/index.js',
  output: {
    filename: 'js/build.[contenthash:10].js',
    path: resolve('build')
  },
  module: {
    ...
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'css/build.[contenthash:10].css'
    }),
    ...
  ],
  mode: 'production'
}
~~~

这样只需要控制hash值的变化来控制资源更新

> babel缓存：让第二次打包构建速度更快
>
> 文件资源缓存：让代码上线运行缓存更好用

## 20、tree shaking

> tree shaking: 去除无用代码

使用前提：

1、必须使用es6模块化

~~~javascript
// test.js
export function a(x, y) {
  return x * y;
}

export function b(x, y) {
  return x - y;
}

// index.js
import { a } from './test'
console.log(a(2,3))
~~~

2、开启production环境

~~~javascript
mode: 'production'
~~~



> 作用：减少代码体积

满足以上条件后重新构建打包，在打包过程中会自动启动tree shaking。打包完成后查看build.js会发现test.js内b方法的相关内容没有了



使用的时候还要注意一个小问题，在不同版本tree shaking会有差异，会无意之间将css文件当做为经引用的代码干掉

模拟测试，在package.json中添加**"sideEffects": false**

> "sideEffects": false : 所有代码都是没有副作用的代码（都可以进行tree shaking）

再构建一次，会发现没有css资源，这样写会把css资源干掉

解决办法： **"sideEffects": ["*.css"]**



## 21、code split（代码分割）

> code spli：将打包输出的一个文件（chunk）分割成多个文件

webpack代码分割大致分为三种方式：1、定义多入口文件。2、使用optimization配置插件。3、通过js

代码让某个文件被单独打包成一个chunk

**1、定义多入口文件**

​	entry以往的使用是写一个入口文件地址

~~~javascript
entry: './src/index.js'
~~~

如上就是配置单入口，倘若在index.js文件中引入其他文件，那么最终打包只会生产一个文件

~~~javascript
// index.js
import test from './test'
console.log('hello index')
~~~

通过entry配置多入口文件

~~~javascript
entry: {
    main: './src/index.js',
    test: './src/test.js'
}
~~~

如上配置多入口文件，在运行打包时，会看到打包出了两个文件，同时也不需要在入口文件中引入test.js文件

打包后的两个文件都是build.hash.js比较难区分，可以同配置output来设置文件名

~~~javascript
output: {
    // 设置[name],取文件名
    filename: 'js/[name].[contenthash:10].js',
    path: resolve('build')
}
~~~

**2、使用optimization配置插件**

作用：可以将node_modules中的代码单独打包成一个chunk输出；自动分析多入口chunk中，有没有公共的文件，如果有会打包成单独一个chunk，不会重复打包

~~~javascript
plugin: [...],
optimization: {
         splitChunks:{
         	chunks: 'all'
         }
}
~~~

**3、通过写js代码的方式打包**

如果entry只设置了一个入口，在单入口文件中通写入js代码(import)的方式引导打包

~~~javascript
//index.js
import('./test') // 这里返回的是promise对象
	.then((res) => {
    	// res为test导出对象结果
    	console.log('导入成功')
	})
	.catch(() => {
    	console.log('导入失败')
	})
~~~

如上配置，运行打包后，也会各自生产两个打包文件，还要一个命名问题，test打包生成的文件名是根据打包顺序命名的可能为123.hash.js。可以通过import内添加注释，设置文件名

~~~javascript
import(/* webpackChunkName: 'test' */'./test')
~~~

如上配置运行打包后，打包文件就会是名为test的文件



## 22、懒加载和预加载

懒加载和预加载通过js代码实现，与webpack配置无关，与代码分割（code split）有一定的相似之处

正常情况下

~~~javascript
// index.js
console.log('index.js文件被加载')
import {a} from './test.js'
console.log(a)
~~~

~~~javascript
export function a(x, y) {
  return x * y;
}
console.log('test.js被加载')
~~~

运行打包后看下，两个文件被先后加载：![4](F:\我创建的例子项目\webpack笔记\4.PNG)

懒加载: 当文件需要使用时才加载

~~~html
<button id="btn"> 点击 </button>
~~~

~~~javascript
//index.js
console.log('index.js文件被加载')
document.getElementById('btn').onclick = function() {
    import(/* webpackChunkName: 'test' */'./test').then(({a}) => {
        console.log(a(2,3))
    }）
}
~~~

在运行打包后，可以看到被index,test被打包成两个chunk，类似代码分割，只有在点击事件生效时，才会加载test文件，重复点击时，也只会加载一次，方法同样事项



预加载：会在使用之前，提前加载js文件

加上一个配置webpackPrefetch

~~~javascript
//index.js
console.log('index.js文件被加载')
document.getElementById('btn').onclick = function() {
    import(/* webpackChunkName: 'test',webpackPrefetch: true */'./test').then(({a}) => {
        console.log(a(2,3))
    }）
}
~~~

打包后打开html文件，会发现点击事件没有生效，但是已经加载了，点击事件生效后，从缓存中拿test文件使用，不会重新加载



> 正常加载可以认为是并行加载（同一时间加载多个文件）
>
> 预加载：prefetch: 等其他资源加载完毕，浏览器空闲了，在偷偷加载。（预加载存在很大兼容问题，最好只在高版本或者移动端使用）



## 23、PWA

> PWA: 渐进式网络开发应用（离线可访问）使用serviceworker实现

webpack实现PWA需要借助一个插件**workbox-webpack-plugin**

npm安装后在webpack中配置

~~~javascript
const WorkboxWebpackPlugin = require('workbox-webpack-plugin')
...
Plugins: [
    ...
    new WorkboxWebpackPlugin.GenerateSW({
        /* 
        1.帮助serviceworker快速启动
        2.删除旧的serviceworker

        生成一个serviceworker 配置文件 ~
      */
     clientsClaim: true,
     skipWaiting: true
    })
]
~~~

webpack文件配置好后，配置入口文件注册serviceWorker

~~~javascript
//index.js
// 应为serviceworker有兼容性问题，以下是简单兼容性处理，是否支持serviceworker
if('serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('/service-worker.js')
      .then(() => {
        console.log('sw注册成功')
      })
      .catch(() => {
        console.log('sw注册失败')
      })
  })
}
~~~

> 注意：eslint 不认识window、navigator 全局变量
>
> 解决： 需要修改package.json 中eslintConfig配置
>
>   "env": {
>
>    	"browser": true // 支持浏览器端全局变量
>
>   }
>
> 
>
> sw代码必须运行在服务器上：两种运行服务方法
>
>    ---> nodejs
>
>    ---> 
>
> ​    npm i server -g
>
> ​    serve -s build 启动一个服务器，将build目录下所有资源作为静态资源暴露出去

如上就完成了pwa的简单配置



## 24、多进程打包

js主线程是单线程的，它同一时间只能干一件事，事情比较多，就要排队等前一个任务结束，在继续；所以通过多进程来优化打包速度

首先需要下载一个loader**thread-loader**

thread-loader放在某一个loader的后面，就会对其开启多进程打包

thread-loader一般是给babel-loader使用,使用方式如下



~~~javascript
//webpack.config.js
{
            test: /\.js$/,
            exclude: /node_modules/,
            use: [
                // 开启多进程打包（babel工作的时候就会开启多进程）
                /*
                	开启多进程打包是有利有弊的（合理使用）
                	进程启动大概600ms,进程通信也有开销（时间）
                	只有工作消耗时间比较长，才需要多进程
                	一般来说js代码比较多，消耗时间比较长
                	
                	启动进程数（cpu核数-1）
                	
                */
                //'thread-loader',
                //如下可做调整
                {
                    loader: 'thread-loader',
                    options： {
                		workers: 2 // 进程2个
                	}
                },
                {
                    loader: 'babel-loader',
                    options: {
                      presets: [
                          [
                              '@babel/preset-env',
                              {
                                  // 按需加载
                                  useBuiltIns: 'usage',
                                  corejs: {version: 3},
                                  targets: { chrome: '60' }
                              }
                          ]
                        ],
                        // 开启babel缓存
                        // 第二次构建时，会读取之前的缓存
                        cacheDirectory: true
                     }
            	]
            }
          }
~~~

## 25、externals

> 作用：防止将某一些包打包到我们最终输出的**bundle**中

假设通过cdn链接引入jQuery，可以通过externals禁止，不会被打包了

~~~javascript
//webpack.config.js
mode: 'production',
externals: {
    // 拒绝jqeruy包被打包进来
    // 忽略库名: 'npm包名'
    jquery: 'jQuery'
}
~~~

## 25、dll(动态链接库)

类似externals，会指示webpack那些库是不参与打包的，不同的是dll会单独对某些库进行单独打包，将多个库打包成一个chunk

node_modules内的某些库比较大，正常打包的话会被打包成一个文件，这样文件体积增大。通过dll将这些库单独拆开，打包成不同的chunk，更有利于性能优化

dll打包后能，webpack运行打包后不会重复打包第三方依赖库，提高效率

首先定义一个webpack.dll.js

~~~javascript
//webpack.dll.js
/* 
  使用dll技术，对某些库（第三方库：jq,react,vue）进行单独打包

    当你运行 webpack 的时候，默认查找 webpack.config.js 配置文件
    需求： 运行 webpack.dll.js 文件
        --> webpack --config webpack.dll.js
*/
const path = require('path')
const webpack = require('webpack')

function resolve(dir) {
  return path.join(__dirname,dir)
}
module.exports = {
  entry: {
    // 最终打包生成 [name] ---> jquery
    // ['jquery',...] ---> 要打包的库是jquery
    jquery: ['jquery']
  },
  output: {
    filename: '[name].js',
    path: resolve('dll'),
    library: '[name]_[hash]' // 打包的库里面向外暴露出去的内容叫什么名字
  },
  plugins:[
    // 打包生成 manifest.json ----> 提供和jquery的映射关系（通过这个映射关系知道jquery不需要打包）
    new webpack.DllPlugin({
      name: '[name]_[hash]', // 映射库的暴露的内容名称
      path: resolve('dll/manifest.json') // 输出的名称
    })
  ],
  mode: 'production'
}
~~~

其次配置webpack.config.js，告诉它那些包不参与打包

~~~javascript
// webpack.config.js
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const webpack = require('webpack')
const AddAssetHtmlWebpackPlugin = require('add-asset-html-webpack-plugin')

function resolve(dir) {
  return path.join(__dirname,dir)
}

plugins: [
    // 处理html结构
    new HtmlWebpackPlugin({
      template: './src/index.html'
    }),
    // 告诉webpack那些库不参与打包，同时使用时的名称也要变
    new webpack.DllReferencePlugin({
      manifest: resolve('dll/manifest.json')
    }),
    // 将某个文件打包输出出去，并在html中自动引入该资源
    new AddAssetHtmlWebpackPlugin({
      filepath: resolve('dll/jquery.js')
    })
  ],
~~~

## 26、webpack配置详解（entry）

~~~javascript
const path = require('path')
const HtmlWebpackPlugin =require('html-webpack-plugin')

function resolve(dir) {
  return path.resolve(__dirname,dir)
}

/* 
  entry: 入口起点
    1.string   --->'./src/index'
      单入口
      打包生成一个chunk，输出一个bundle文件
      此时chunk的名称默认是 main
    2、array ----> ['./src/index','./src/add.js']
      多入口
      所有入口文件最终只会形成一个chunk，输出出去一个bundle文件。
        只有在HMR功能中让html热跟新生效~
    3、object  ----> ['./src/index','./src/add.js']
      多入口
      有几个入口文件就形成几个chunk，同时输出几个bundle
      此时chunk名称是 key

        特殊用法：
          {
            // 所有入口文件最终只会形成一个chunk，输出出去一个bundle文件。
            index: ['./src/index', './src/count.js'],
            // 形成一个chunk ，输出一个bundle文件
            add: './src/add.js'
          }

*/

module.exports = {
  // entry: './src/index',
  // entry: ['./src/index','./src/add.js'],
  entry: {
    index: ['./src/index', './src/count.js'],
    add: './src/add.js'
  },
  output: {
    filename:'[name].js',
    path: resolve('build')
  },
  plugins: [
    new HtmlWebpackPlugin()
  ],
  mode: 'development'
}
~~~

## 27、webpack配置详解（output）

~~~javascript
const path = require('path')
const HtmlWebpackPlugin =require('html-webpack-plugin')

function resolve(dir) {
  return path.resolve(__dirname,dir)
}

module.exports = {
  entry: './src/index',
  output: {
    // 文件名称（指定名称+目录）
    filename:'js/[name].js',
    // 输出文件目录（将来所有资源输出的公共目录）
    path: resolve('build'),
    // 所有资源引入公共路径前缀  --> 'imags/a.ipg' --> '/imags/a.jpg'
    publicPath: '/',
    chunkFilename: '[name]_chunk.js', // 非入口chunk的名称
    // library: '[name]', // 整个库向外暴露的变量名  通常结合dll使用
    // libraryTarget: 'window' // 变量名添加到那个上 browser
    // libraryTarget: 'global' // 变量名添加到那个上 node
    // libraryTarget: 'commonjs'
  },
  plugins: [
    new HtmlWebpackPlugin()
  ],
  mode: 'development'
}
~~~

## 28、webpack配置详解（module）

~~~javascript
const path = require('path')
const HtmlWebpackPlugin =require('html-webpack-plugin')

function resolve(dir) {
  return path.resolve(__dirname,dir)
}

module.exports = {
  entry: './src/index',
  output: {
    filename:'js/[name].js',
    path: resolve('build')
  },
  module: {
    rules: [
      // loader配置
      {
        test: /\.css$/,
        // 使用多个louder
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.js$/,
        // 排除node_modules 下的js文件
        exclude: /node_modules/,
        // 只检查src下的js文件
        include: resolve('src'),
        enforce: 'pre', // 优先执行
        // enforce: 'post', // 延后执行
        // 使用一个louder
        loader: 'eslint-loader',
        options: {}
      },
      {
        // 一下配置只会生效一个
        oneOf: []
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin()
  ],
  mode: 'development'
}
~~~

## 29、webpack配置详解（resolve）

~~~javascript
const path = require('path')
const HtmlWebpackPlugin =require('html-webpack-plugin')

function resolve(dir) {
  return path.resolve(__dirname,dir)
}

module.exports = {
  entry: './src/js/index.js',
  output: {
    filename:'js/[name].js',
    path: resolve('build')
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin()
  ],
  mode: 'development',
  // 解析模块的规则
  resolve: {
      // 配置解析模块路基别名:简写路径 缺点写路基没有提示
      /*
      	//index.js
      	import './css/index.css' => import '$css/index.css'
      */
    alias: {
      $css: resolve('src/css')
    },
    // 配置省略文件路径后缀名
      /*
      	//index.js
      	import './css/index.css' => import './css/index'
      */
    extensions: ['.js','.json','.jsx', '.css'],
    // 告诉webpack 解析模块时去找那个目录
    modules: [resolve('../../node_modules'),'node_modules']
  }
}
~~~

## 30、devServer

~~~javascript
const path = require('path')
const HtmlWebpackPlugin =require('html-webpack-plugin')

function resolve(dir) {
  return path.resolve(__dirname,dir)
}

module.exports = {
  entry: './src/js/index.js',
  output: {
    filename:'js/[name].js',
    path: resolve('build')
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin()
  ],
  mode: 'development',
  /* 
    开发服务器 devServer :用来自动化（自动编译，自动打开浏览器，自动刷新浏览器~）
    特点：只会在内存中打包，不会有任何输出
    启动devServer指令为  npx webpack-dev-server
  */
  devServer: {
    // 项目构建后路径  运行代码目录
    contentBase: resolve('build'),
    // 见识 contentBase 目录下的所有文件，一旦文件变化就会 reload
    watchContentBase: true,
    watchOptions: {
      // 忽略文件
      ignored: /node_modules/
    },
    // 启动gzip压缩
    compress: true,
    // 端口号
    port: 3000,
    // 域名
    host: 'localhost',
    // 自动打开浏览器
    open: true,
    // 开启HMR功能
    hot: true,
    // 不要显示启动服务器日志详细
    clientLogLevel: 'none',
    // 除了一些基本启动信息以外，其他内容都不要显示
    quiet:true,
    // 如果出错了，不要全屏提示~
    overlay: false,
    // 服务器代理  --> 解决开发环境跨域问题
    proxy: {
      // 一旦devServer（5000）服务器接收到 /api/xxx 的请求，就会吧请求转发到另外一个服务器（3000）
      '/api': {
        target: 'http://localhost:3000',
        // 发送请求是，请求路径重写：将 /api/xxx ---> /xxx (去掉api)
        parthRewrite: {
          '^/api': ''
        }
      }
    }
  }
}
~~~

## 31 、optimization

~~~javascript
const path = require('path')
const HtmlWebpackPlugin =require('html-webpack-plugin')
const TerserWebpackPlugin = require('terser-webpack-plugin')

function resolve(dir) {
  return path.resolve(__dirname,dir)
}

module.exports = {
  entry: './src/js/index.js',
  output: {
    filename:'js/[name].[contenthash:10].js',
    path: resolve('build')
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin()
  ],
  mode: 'production',
  // 解析模块的规则
  resolve: {
    // 配置解析模块路基别名:简写路径 缺点写路基没有提示
    alias: {
      $css: resolve('src/css')
    },
    // 配置省略文件路径后缀名
    extensions: ['.js','.json','.jsx', '.css'],
    // 告诉webpack 解析模块时去找那个目录
    modules: [resolve('../../node_modules'),'node_modules']
  },
  optimization: {
    splitChunks: { //用来做代码分割
      chunks: 'all',
      // 默认值， 可以不写
      /*minSize: 30 * 1024, // 分割的chunk最小为30kb
       maxSize: 0, // 最大没有限制
      minChunks: 1, // 要提货的chunk最少要被应用一次
      maxAsyncRequests: 5, // 按需加载时并行加载的文件最大数量是5
      maxInitialRequests: 3, // 入口js最大并行请求数量
      automaticNameDelimiter: '~', // 名称链接符
      name: true, // k可以使用命名规则
      cacheGroups: { // 分割chunk的组
        // node_modules 文件会被打包到 vendoes 组的chunk中。 ---> vendors~xxx.js
        // 满足上面公共规则：如：大小超过30kb，最少被应用一次
        vendors: {
          test:/[\\/]node_modules[\\/]/,
          // 优先级
          priority: -10
        },
        default: {
          // 要提取的chunk 最少被引用2次
          minChunks: 2,
          priority: -10,
          // 如果当前要打包的这个模块和之前已经被提取的某块是同一个就会服用，而不是重新打包模块
          reuseExistingChunk: true
        }
      } */
    },
    // 将当前模块的的记录其他模块的hash单独打包为一个文件 runtime
    // 解决 修改a文件导致把文件的contenthash变化
    runtimeChunk: {
      name: entrypoint => `runtime-${entrypoint.name}`
    },
    minimize:true,
    minimizer: [
      // 配置生产环境的压缩方案：js和css
      new TerserWebpackPlugin({
        test: /\.js(\?.*)?$/i
        // cache: true, // 开启缓存
        // parallel: true, //开启多进程打包
        // // 启动source-map
        // sourceMap: true
      })
    ]
  }
}
~~~

